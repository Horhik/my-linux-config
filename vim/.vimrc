syntax enable
syntax on
set nocompatible
set relativenumber
set noshowmode
set laststatus=2
set softtabstop=2
set tabstop=2
set guifont=Hack\ 14
set encoding=UTF-8
let g:molokai_original = 1


" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')
Plug 'fatih/vim-go', { 'tag': '*' }

Plug 'mboughaba/i3config.vim'
Plug 'neovimhaskell/haskell-vim'
Plug 'gchrisdone/hindent'
Plug 'gjaspervdj/stylish-haskell'
Plug 'jaspervdj/stylish-haskell'
Plug 'chrisdone/hindent'
Plug 'dense-analysis/ale'
Plug 'itchyny/lightline.vim'
Plug 'terryma/vim-multiple-cursors'
Plug 'tpope/vim-surround'
Plug 'preservim/nerdtree'
Plug 'phanviet/vim-monokai-pro'
Plug 'sonph/onehalf', {'rtp': 'vim/'}
Plug 'sheerun/vim-polyglot'
Plug 'ayu-theme/ayu-vim' " or other package manager
Plug 'sickill/vim-monokai'
Plug 'yarisgutierrez/ayu-lightline'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'ryanoasis/vim-devicons'
Plug 'prettier/vim-prettier', { 'do': 'yarn install' }
call plug#end()
aug i3config_ft_detection
	  au!
	    au BufNewFile,BufRead ~/.config/i3/config set filetype=i3config
aug end

if !has('gui_running')
  set t_Co=256
endif
let g:lightline = {
      \ 'colorscheme': 'ayu',
      \ }


set termguicolors     " enable true colors support
" let ayucolor="mirage" " for mirage version of theme
let ayucolor="dark"   " for dark version of theme
colorscheme ayu


"keybuildings"
map <C-o> :NERDTreeToggle<CR>


" Max line length that prettier will wrap on: a number or 'auto' (use
" textwidth).
" default: 'auto'
let g:prettier#config#print_width = 'auto'

" number of spaces per indentation level: a number or 'auto' (use
" softtabstop)
" default: 'auto'
let g:prettier#config#tab_width = 'auto'

" use tabs instead of spaces: true, false, or auto (use the expandtab setting).
" default: 'auto'
let g:prettier#config#use_tabs = 'auto'

" flow|babylon|typescript|css|less|scss|json|graphql|markdown or empty string
" (let prettier choose).
" default: ''
let g:prettier#config#parser = ''

" cli-override|file-override|prefer-file
" default: 'file-override'
let g:prettier#config#config_precedence = 'file-override'

" always|never|preserve
" default: 'preserve'
let g:prettier#config#prose_wrap = 'preserve'

" css|strict|ignore
" default: 'css'
let g:prettier#config#html_whitespace_sensitivity = 'css'

" false|true
" default: 'false'
let g:prettier#config#require_pragma = 'false'

" Prettier 
let g:prettier#autoformat = 1
let g:prettier#exec_cmd_path = "/usr/bin/prettier"
let g:prettier#config#trailing_comma = 'none'


